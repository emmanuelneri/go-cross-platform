release-local:
	goreleaser release --snapshot --rm-dist

# https://github.com/sickcodes/Docker-OSX
# https://github.com/sickcodes/Docker-OSX/issues/498
# test-macos-intel:
# 	@docker 

test-ubuntu-amd64:
	@docker build -t go-cross-platform/ubuntu-amd64 -f scripts/ubuntu/Dockerfile --no-cache .  
	@docker run -it go-cross-platform/ubuntu-amd64 version
