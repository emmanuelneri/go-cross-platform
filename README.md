# go-releaser

## Gitlab Token


Define an user token in `GITLAB_TOKEN` (project -> Settings -> CI/CD Settings)


### sign 
https://www.gnupg.org/download/
https://docs.github.com/en/authentication/managing-commit-signature-verification/generating-a-new-gpg-key

gpg --default-new-key-algo rsa4096 --gen-key
gpg --list-secret-keys --keyid-format=long
gpg --armor --export *****  ## GPG_PUBLIC_KEY

Copy exported value and define GPG_PUBLIC_KEY (project -> Settings -> CI/CD Settings)
Define password created in `gen-key` GPG_PASSWORD (project -> Settings -> CI/CD Settings)